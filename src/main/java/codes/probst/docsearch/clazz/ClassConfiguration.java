package codes.probst.docsearch.clazz;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import codes.probst.docsearch.clazz.dao.ClassDao;
import codes.probst.docsearch.clazz.dao.ClassUsageDao;
import codes.probst.docsearch.clazz.dao.JpaClassDao;
import codes.probst.docsearch.clazz.dao.JpaClassUsageDao;
import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.facade.ClassFacade;
import codes.probst.docsearch.clazz.facade.ClassPopulationOption;
import codes.probst.docsearch.clazz.facade.DefaultClassFacade;
import codes.probst.docsearch.clazz.facade.SpringClassFacade;
import codes.probst.docsearch.clazz.facade.converter.DefaultClassConverter;
import codes.probst.docsearch.clazz.facade.populator.ClassDescriptionPopulator;
import codes.probst.docsearch.clazz.facade.populator.ClassMethodsPopulator;
import codes.probst.docsearch.clazz.facade.populator.ClassPackagePopulator;
import codes.probst.docsearch.clazz.facade.populator.ClassProjectPopulator;
import codes.probst.docsearch.clazz.facade.populator.ClassShortDescriptionPopulator;
import codes.probst.docsearch.clazz.facade.populator.ClassSourcePopulator;
import codes.probst.docsearch.clazz.facade.populator.ClassUrlPopulator;
import codes.probst.docsearch.clazz.facade.populator.ClassVersionPopulator;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.service.ClassService;
import codes.probst.docsearch.clazz.service.DefaultClassService;
import codes.probst.docsearch.clazz.url.DefaultClassUrlResolver;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.converter.Converter;
import codes.probst.framework.populator.ConfigurablePopulator;
import codes.probst.framework.populator.DefaultConfigurablePopulator;
import codes.probst.framework.populator.PopulationOption;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlCleaner;
import codes.probst.framework.url.UrlResolver;

public class ClassConfiguration {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UrlCleaner urlCleaner;
	
	@Autowired
	private BeanResolver beanResolver;
	
	@Bean
	public ClassDao classDao() {
		JpaClassDao dao = new JpaClassDao();
		
		dao.setEntityManager(entityManager);
		
		return dao;
	}
	
	@Bean
	public ClassUsageDao classUsageDao() {
		JpaClassUsageDao dao = new JpaClassUsageDao();
		
		dao.setEntityManager(entityManager);
		
		return dao;
	}
	
	@Bean
	public ClassService classService() {
		DefaultClassService service = new DefaultClassService();
		
		service.setDao(classDao());
		service.setClassUsageDao(classUsageDao());
		
		return service;
	}
	
	@Bean
	public ClassFacade classFacade() {
		DefaultClassFacade facade = new SpringClassFacade();
		
		facade.setService(classService());
		facade.setConverter(classConverter());
		facade.setPopulator(classPopulator());
		
		return facade;
	}
	
	@Bean
	public Converter<ClassModel, ClassData> classConverter() {
		DefaultClassConverter converter = new DefaultClassConverter();
		
		return converter;
	}
	
	@Bean
	public ConfigurablePopulator<ClassModel, ClassData> classPopulator() {
		DefaultConfigurablePopulator<ClassModel, ClassData> populator = new DefaultConfigurablePopulator<>();
		
		populator.setPopulators(classPopulators());
		
		return populator;
	}
	
	@Bean
	public Map<PopulationOption, Collection<Populator<ClassModel, ClassData>>> classPopulators() {
		Map<PopulationOption, Collection<Populator<ClassModel, ClassData>>> map = new HashMap<>();
		
		map.put(ClassPopulationOption.OPTION_SHORT_DESCRIPTION, Arrays.asList(
			classShortDescriptionPopulator()
		));
		map.put(ClassPopulationOption.OPTION_URL, Arrays.asList(
			classUrlPopulator()
		));
		map.put(ClassPopulationOption.OPTION_PROJECT, Arrays.asList(
			classProjectPopulator()
		));
		map.put(ClassPopulationOption.OPTION_VERSION, Arrays.asList(
			classVersionPopulator()
		));
		map.put(ClassPopulationOption.OPTION_DESCRIPTION, Arrays.asList(
			classDescriptionPopulator()
		));
		map.put(ClassPopulationOption.OPTION_SOURCE, Arrays.asList(
			classSourcePopulator()
		));
		map.put(ClassPopulationOption.OPTION_PACKAGE, Arrays.asList(
			classPackagePopulator()
		));
		map.put(ClassPopulationOption.OPTION_METHODS, Arrays.asList(
			classMethodsPopulator()
		));
		
		
		return map;
	}
	
	@Bean
	public Populator<ClassModel, ClassData> classShortDescriptionPopulator() {
		ClassShortDescriptionPopulator populator = new ClassShortDescriptionPopulator();
		
		populator.setSentences(1);
		
		return populator;
	}
	
	@Bean
	public UrlResolver<ClassModel> classUrlResolver() {
		DefaultClassUrlResolver resolver = new DefaultClassUrlResolver();
		
		resolver.setUrlCleaner(urlCleaner);
		
		return resolver;
	}
	
	@Bean
	public Populator<ClassModel, ClassData> classUrlPopulator() {
		ClassUrlPopulator populator = new ClassUrlPopulator();
		
		populator.setResolver(classUrlResolver());
		
		return populator;
	}
	
	@Bean
	public Populator<ClassModel, ClassData> classProjectPopulator() {
		ClassProjectPopulator populator = new ClassProjectPopulator();
		
		populator.setBeanResolver(beanResolver);
		
		return populator;
	}

	@Bean
	public Populator<ClassModel, ClassData> classVersionPopulator() {
		ClassVersionPopulator populator = new ClassVersionPopulator();

		populator.setBeanResolver(beanResolver);
		
		return populator;
	}

	@Bean
	public Populator<ClassModel, ClassData> classDescriptionPopulator() {
		ClassDescriptionPopulator populator = new ClassDescriptionPopulator();

		return populator;
	}

	@Bean
	public Populator<ClassModel, ClassData> classSourcePopulator() {
		ClassSourcePopulator populator = new ClassSourcePopulator();

		return populator;
	}
	
	@Bean
	public Populator<ClassModel, ClassData> classPackagePopulator() {
		ClassPackagePopulator populator = new ClassPackagePopulator();
		
		populator.setBeanResolver(beanResolver);
		
		return populator;
	}
	
	@Bean
	public Populator<ClassModel, ClassData> classMethodsPopulator() {
		ClassMethodsPopulator populator = new ClassMethodsPopulator();
		
		populator.setBeanResolver(beanResolver);
		
		return populator;
	}
}
