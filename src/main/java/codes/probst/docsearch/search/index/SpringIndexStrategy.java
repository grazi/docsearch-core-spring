package codes.probst.docsearch.search.index;

import org.springframework.transaction.annotation.Transactional;

public class SpringIndexStrategy extends DefaultIndexStrategy {

	@Transactional
	@Override
	public void index(IndexType type) {
		super.index(type);
	}
}
