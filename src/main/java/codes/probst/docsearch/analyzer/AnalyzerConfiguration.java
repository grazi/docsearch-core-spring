package codes.probst.docsearch.analyzer;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import codes.probst.docsearch.analyzer.projectinformation.DefaultProjectInformationAnalyzer;
import codes.probst.docsearch.analyzer.projectinformation.JavaProjectInformationEvaluator;
import codes.probst.docsearch.analyzer.projectinformation.MavenProjectTypeEvaluator;
import codes.probst.docsearch.analyzer.projectinformation.ProjectInformationAnalyzer;
import codes.probst.docsearch.analyzer.projectinformation.ProjectInformationEvaluator;
import codes.probst.docsearch.analyzer.service.ClassAnalyzerService;
import codes.probst.docsearch.analyzer.service.CodeSnippedService;
import codes.probst.docsearch.analyzer.service.DefaultClassAnalyzerService;
import codes.probst.docsearch.analyzer.service.DefaultCodeSnippedService;
import codes.probst.framework.file.service.FileService;

@Configuration
public class AnalyzerConfiguration {
	@Autowired
	private FileService fileService;
	
	@Bean
	public ProjectInformationAnalyzer projectInformationAnalyzer() {
		DefaultProjectInformationAnalyzer analyzer = new DefaultProjectInformationAnalyzer();
		
		analyzer.setEvaluators(projectInformationEvaluators());
		analyzer.setFileService(fileService);
		
		return analyzer;
	}
	
	@Bean
	public Collection<ProjectInformationEvaluator> projectInformationEvaluators() {
		Collection<ProjectInformationEvaluator> evaluators = new ArrayList<>(1);
		
		evaluators.add(javaProjectInformationEvaluator());
		evaluators.add(mavenProjectInformationEvaluator());
		
		return evaluators;
	}
	
	@Bean
	public ProjectInformationEvaluator javaProjectInformationEvaluator() {
		JavaProjectInformationEvaluator evaluator = new JavaProjectInformationEvaluator();
		
		return evaluator;
	}
	
	@Bean
	public ClassAnalyzerService classAnalyzerService() {
		DefaultClassAnalyzerService service = new DefaultClassAnalyzerService();
		
		return service;
	}
	
	@Bean
	public ProjectInformationEvaluator mavenProjectInformationEvaluator() {
		MavenProjectTypeEvaluator evaluator = new MavenProjectTypeEvaluator();
		
		return evaluator;
	}

	@Bean
	public CodeSnippedService codeSnippedService() {
		DefaultCodeSnippedService service = new DefaultCodeSnippedService();
		
		service.setMaximumNumberOfLines(10);
		
		return service;
	}
	
}
