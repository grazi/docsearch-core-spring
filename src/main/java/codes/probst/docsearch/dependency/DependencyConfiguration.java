package codes.probst.docsearch.dependency;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.maven.model.Repository;
import org.apache.maven.model.resolution.ModelResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.dependency.maven.MavenDependencyResolver;
import codes.probst.docsearch.dependency.maven.RepositoryModelResolver;
import codes.probst.docsearch.dependency.service.DefaultDependencyService;
import codes.probst.docsearch.dependency.service.DependencyService;
import codes.probst.docsearch.version.service.VersionService;
import codes.probst.framework.file.service.FileService;

@Configuration
public class DependencyConfiguration {
	@Autowired
	private FileService fileService;
	
	@Autowired
	private VersionService versionService;
	
	@Bean
	public DependencyService DependencyService() {
		DefaultDependencyService service = new DefaultDependencyService();
		
		service.setResolvers(dependencyResolvers());
		service.setVersionService(versionService);
		
		return service;
	}
	
	@Bean
	public Map<ProjectType, DependencyResolver> dependencyResolvers() {
		Map<ProjectType, DependencyResolver> resolvers = new HashMap<>();

		resolvers.put(ProjectType.JAVA, javaDependencyResolver());
		resolvers.put(ProjectType.PLAIN_JAVA, javaDependencyResolver());
		resolvers.put(ProjectType.MAVEN, mavenDependencyResolver());
		
		return resolvers;
	}
	
	@Bean
	public DependencyResolver javaDependencyResolver() {
		NoopDependencyResolver resolver = new NoopDependencyResolver();
		
		return resolver;
	}
	
	@Bean
	public DependencyResolver mavenDependencyResolver() {
		MavenDependencyResolver resolver = new MavenDependencyResolver();
		
		resolver.setModelResolver(mavenModelResolver());
		resolver.setFileService(fileService);
		
		return resolver;
	}
	
	@Bean
	public ModelResolver mavenModelResolver() {
		RepositoryModelResolver resolver = new RepositoryModelResolver();
		
		resolver.setRepositories(mavenRepositories());
		
		return resolver;
	}
	
	@Bean
	public List<Repository> mavenRepositories() {
		List<Repository> repositories = new ArrayList<>();
		
		
		Properties properties = mavenProperties();
		properties.stringPropertyNames().stream().sorted(String::compareTo).forEach(key -> {
			if(key.startsWith("maven.")) {
				String id = key.substring("maven.".length());
				id = id.substring(id.indexOf(".") + 1);
				String url = properties.getProperty(key);
				
				Repository repository = new Repository();
				repository.setId(id);
				repository.setUrl(url);
				
				repositories.add(repository);
			}
		});
		
		return repositories;
	}
	
	@Bean
	public Properties mavenProperties() {
		Properties properties = new Properties();
		
		try {
			properties.load(getClass().getResourceAsStream("/maven.properties"));
		} catch (IOException e) {
			throw new RuntimeException("could not load properties", e);
		}
		
		return properties;
	}
}
