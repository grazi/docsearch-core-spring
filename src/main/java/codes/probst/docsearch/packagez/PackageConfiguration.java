package codes.probst.docsearch.packagez;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import codes.probst.docsearch.packages.dao.JpaPackageDao;
import codes.probst.docsearch.packages.dao.PackageDao;
import codes.probst.docsearch.packages.facade.DefaultPackageFacade;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.facade.PackageFacade;
import codes.probst.docsearch.packages.facade.PackagePopulationOption;
import codes.probst.docsearch.packages.facade.SpringPackageFacade;
import codes.probst.docsearch.packages.facade.converter.DefaultPackageConverter;
import codes.probst.docsearch.packages.facade.populator.PackageClassesPopulator;
import codes.probst.docsearch.packages.facade.populator.PackageDocumentationPopulator;
import codes.probst.docsearch.packages.facade.populator.PackageShortDocumentationPopulator;
import codes.probst.docsearch.packages.facade.populator.PackageUrlPopulator;
import codes.probst.docsearch.packages.facade.populator.PackageVersionPopulator;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.service.DefaultPackageService;
import codes.probst.docsearch.packages.service.PackageService;
import codes.probst.docsearch.packages.url.DefaultPackageUrlResolver;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.converter.Converter;
import codes.probst.framework.populator.ConfigurablePopulator;
import codes.probst.framework.populator.DefaultConfigurablePopulator;
import codes.probst.framework.populator.PopulationOption;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlCleaner;
import codes.probst.framework.url.UrlResolver;

public class PackageConfiguration {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UrlCleaner urlCleaner;
	
	@Autowired
	private BeanResolver beanResolver;
	
	@Bean
	public PackageDao packageDao() {
		JpaPackageDao dao = new JpaPackageDao();
		
		dao.setEntityManager(entityManager);
		
		return dao;
	}
	
	@Bean
	public PackageService packageService() {
		DefaultPackageService service = new DefaultPackageService();
		
		service.setDao(packageDao());
		
		return service;
	}
	
	@Bean
	public PackageFacade packageFacade() {
		DefaultPackageFacade facade = new SpringPackageFacade();
		
		facade.setConverter(packageConverter());
		facade.setPopulator(packagePopulator());
		facade.setService(packageService());
		
		return facade;
	}
	
	@Bean
	public Converter<PackageModel, PackageData> packageConverter() {
		DefaultPackageConverter converter = new DefaultPackageConverter();
		
		return converter;
	}
	
	@Bean
	public ConfigurablePopulator<PackageModel, PackageData> packagePopulator() {
		DefaultConfigurablePopulator<PackageModel, PackageData> populator = new DefaultConfigurablePopulator<>();
		
		populator.setPopulators(packagePopulators());
		
		return populator;
	}
	
	@Bean
	public Map<PopulationOption, Collection<Populator<PackageModel, PackageData>>> packagePopulators() {
		Map<PopulationOption, Collection<Populator<PackageModel, PackageData>>> map = new HashMap<>();
		
		map.put(PackagePopulationOption.OPTION_URL, Arrays.asList(
			packageUrlPopulator()
		));
		map.put(PackagePopulationOption.OPTION_SHORT_DOCUMENTATION, Arrays.asList(
			packageShortDocumentationPopulator()
		));
		map.put(PackagePopulationOption.OPTION_DOCUMENTATION, Arrays.asList(
			packageDocumentationPopulator()
		));
		map.put(PackagePopulationOption.OPTION_VERSION, Arrays.asList(
			packageVersionPopulator()
		));
		map.put(PackagePopulationOption.OPTION_CLASSES, Arrays.asList(
			packageClassesPopulator()
		));
		
		
		
		return map;
	}
	
	@Bean
	public Populator<PackageModel, PackageData> packageUrlPopulator() {
		PackageUrlPopulator populator = new PackageUrlPopulator();
		
		populator.setResolver(packageUrlResolver());
		
		return populator;
	}
	
	@Bean
	public UrlResolver<PackageModel> packageUrlResolver() {
		DefaultPackageUrlResolver resolver = new DefaultPackageUrlResolver();
		
		resolver.setUrlCleaner(urlCleaner);
		
		return resolver;
	}
	
	@Bean
	public Populator<PackageModel, PackageData> packageShortDocumentationPopulator() {
		PackageShortDocumentationPopulator populator = new PackageShortDocumentationPopulator();
		
		populator.setSentences(1);
		
		return populator;
	}
	
	@Bean
	public Populator<PackageModel, PackageData> packageDocumentationPopulator() {
		PackageDocumentationPopulator populator = new PackageDocumentationPopulator();
		
		return populator;
	}
	
	@Bean
	public Populator<PackageModel, PackageData> packageVersionPopulator() {
		PackageVersionPopulator populator = new PackageVersionPopulator();
		
		populator.setBeanResolver(beanResolver);
		
		return populator;
	}
	
	@Bean
	public Populator<PackageModel, PackageData> packageClassesPopulator() {
		PackageClassesPopulator populator = new PackageClassesPopulator();
		
		populator.setBeanResolver(beanResolver);
		
		return populator;
	}
	
}
