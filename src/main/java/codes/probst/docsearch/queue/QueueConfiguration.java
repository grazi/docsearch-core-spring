package codes.probst.docsearch.queue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

import codes.probst.docsearch.analyzer.projectinformation.ProjectInformationAnalyzer;
import codes.probst.docsearch.analyzer.service.ClassAnalyzerService;
import codes.probst.docsearch.analyzer.service.CodeSnippedService;
import codes.probst.docsearch.clazz.service.ClassService;
import codes.probst.docsearch.dependency.service.DependencyService;
import codes.probst.docsearch.method.service.MethodService;
import codes.probst.docsearch.packages.service.PackageService;
import codes.probst.docsearch.project.service.ProjectService;
import codes.probst.docsearch.queue.dao.JpaQueueEntryDao;
import codes.probst.docsearch.queue.dao.QueueEntryDao;
import codes.probst.docsearch.queue.facade.DefaultQueueEntryFacade;
import codes.probst.docsearch.queue.facade.QueueEntryFacade;
import codes.probst.docsearch.queue.facade.SpringQueueEntryFacade;
import codes.probst.docsearch.queue.job.ClassImportPostProcessor;
import codes.probst.docsearch.queue.job.JavaClassImportPostProcessor;
import codes.probst.docsearch.queue.job.SpringQueueEntryCleanupJob;
import codes.probst.docsearch.queue.job.SpringQueueEntryProcessorJob;
import codes.probst.docsearch.queue.service.DefaultQueueEntryProcessor;
import codes.probst.docsearch.queue.service.DefaultQueueEntryService;
import codes.probst.docsearch.queue.service.QueueEntryProcessor;
import codes.probst.docsearch.queue.service.QueueEntryService;
import codes.probst.docsearch.queue.service.SpringQueueEntryProcessor;
import codes.probst.docsearch.version.service.VersionService;
import codes.probst.framework.file.service.FileService;

@Configuration
public class QueueConfiguration {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private ProjectInformationAnalyzer projectInformationAnalyzer;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private VersionService versionService;
	
	@Autowired
	private PackageService packageService;
	
	@Autowired
	private ClassService classService;
	
	@Autowired
	private MethodService methodService;
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private DependencyService dependencyService;
	
	@Autowired
	private ClassAnalyzerService classAnalyzerService;
	
	@Autowired
	private CodeSnippedService codeSnippedService;
	
	@Bean
	public QueueEntryDao queueEntryDao() {
		JpaQueueEntryDao dao = new JpaQueueEntryDao();
		
		dao.setEntityManager(entityManager);
		
		return dao;
	}
	
	@Bean
	public QueueEntryService queueEntryService() {
		DefaultQueueEntryService service = new DefaultQueueEntryService();
		
		service.setDao(queueEntryDao());
		service.setProjectInformationAnalyzer(projectInformationAnalyzer);
		
		return service;
	}
	
	@Bean
	public JobDetail queueEntryProcessorJob() {
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(SpringQueueEntryProcessorJob.class);
		factory.setGroup("queue-entry");
		factory.setName("processor");
		factory.setDurability(true);
		factory.afterPropertiesSet();
		return factory.getObject();
	}
	
	@Bean
	public Trigger queueEntryProcessorJobTrigger() throws ParseException {
		CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
		
		trigger.setCronExpression("0 0/5 * * * ?");
		trigger.setJobDetail(queueEntryProcessorJob());
		trigger.setGroup("queue-entry");
		trigger.setName("processor-trigger");
		trigger.afterPropertiesSet();
		
		return trigger.getObject();
	}
	
	@Bean
	public JobDetail queueEntryCleanupJob() {
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(SpringQueueEntryCleanupJob.class);
		factory.setGroup("queue-entry");
		factory.setName("cleanup");
		factory.setDurability(true);
		factory.afterPropertiesSet();
		return factory.getObject();
	}
	
	@Bean
	public Trigger queueEntryCleanupJobTrigger() throws ParseException {
		CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
		
		trigger.setCronExpression("0 0 2 * * ?");
		trigger.setJobDetail(queueEntryCleanupJob());
		trigger.setGroup("queue-entry");
		trigger.setName("cleanup-trigger");
		trigger.afterPropertiesSet();
		
		return trigger.getObject();
	}
	
	@Bean
	public Collection<ClassImportPostProcessor> classImportPostProcessors() {
		return Arrays.asList(
			javaClassImportPostProcessor()
		);
	}
	
	@Bean
	public ClassImportPostProcessor javaClassImportPostProcessor(){
		JavaClassImportPostProcessor processor = new JavaClassImportPostProcessor();
		
		processor.setClassService(classService);
		processor.setPackageService(packageService);
		
		return processor;
	}
	
	@Bean
	public QueueEntryFacade queueEntryFacade() {
		DefaultQueueEntryFacade facade = new SpringQueueEntryFacade();
		
		facade.setQueueEntryService(queueEntryService());
		
		return facade;
	}
	
	@Bean
	public QueueEntryProcessor queueEntryProcessor() {
		DefaultQueueEntryProcessor processor = new SpringQueueEntryProcessor();
		
		processor.setQueueEntryService(queueEntryService());
		processor.setFileService(fileService);
		processor.setProjectService(projectService);
		processor.setVersionService(versionService);
		processor.setPackageService(packageService);
		processor.setClassService(classService);
		processor.setMethodService(methodService);
		processor.setDependencyService(dependencyService);
		processor.setClassAnalyzerService(classAnalyzerService);
		processor.setCodeSnippedService(codeSnippedService);
		processor.setClassImportPostProcessors(classImportPostProcessors());
		
		
		return processor;
	}
}
