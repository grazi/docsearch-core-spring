package codes.probst.docsearch.queue.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import codes.probst.docsearch.queue.service.QueueEntryService;

public class SpringQueueEntryCleanupJob extends QueueEntryCleanupJob {
	private TransactionTemplate transactionTemplate;
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		transactionTemplate.execute((status) -> {
			try {
				super.execute(arg0);
			} catch (JobExecutionException e) {
				throw new RuntimeException(e);
			}
			return null;
		});
	}
	
	@Autowired
	@Override
	public void setQueueEntryService(QueueEntryService queueEntryService) {
		super.setQueueEntryService(queueEntryService);
	}
	
	@Autowired
	public void setPlatformTransactionManager(PlatformTransactionManager platformTransactionManager) {
		transactionTemplate = new TransactionTemplate(platformTransactionManager);
	}
}
