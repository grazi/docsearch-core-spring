package codes.probst.docsearch.queue.job;

import org.springframework.beans.factory.annotation.Autowired;

import codes.probst.docsearch.queue.service.QueueEntryProcessor;
import codes.probst.docsearch.queue.service.QueueEntryService;

public class SpringQueueEntryProcessorJob extends QueueEntryProcessorJob {


	@Autowired
	@Override
	public void setQueueEntryService(QueueEntryService queueEntryService) {
		super.setQueueEntryService(queueEntryService);
	}

	@Autowired
	@Override
	public void setQueueEntryProcessor(QueueEntryProcessor queueEntryProcessor) {
		super.setQueueEntryProcessor(queueEntryProcessor);
	}

}
