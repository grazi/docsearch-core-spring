package codes.probst.docsearch.queue.service;

import org.springframework.transaction.annotation.Transactional;

import codes.probst.docsearch.queue.model.QueueEntryModel;

public class SpringQueueEntryProcessor extends DefaultQueueEntryProcessor {

	@Transactional
	@Override
	public void process(QueueEntryModel queueEntry) {
		super.process(queueEntry);
	}
}
