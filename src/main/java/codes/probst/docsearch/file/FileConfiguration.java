package codes.probst.docsearch.file;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import codes.probst.framework.file.service.DefaultFileService;

import codes.probst.framework.file.service.FileService;

@Configuration
public class FileConfiguration {

	@Bean
	public FileService fileService() {
		DefaultFileService service = new DefaultFileService();
		
		return service;
	}
}
