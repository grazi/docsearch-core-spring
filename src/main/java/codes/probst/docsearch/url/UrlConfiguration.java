package codes.probst.docsearch.url;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import codes.probst.framework.url.DefaultUrlCleaner;
import codes.probst.framework.url.UrlCleaner;

@Configuration
public class UrlConfiguration {

	@Bean
	public UrlCleaner urlCleaner() {
		DefaultUrlCleaner cleaner = new DefaultUrlCleaner();
		
		return cleaner;
	}
}
