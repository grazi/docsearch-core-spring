package codes.probst.docsearch;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import codes.probst.framework.bean.BeanResolver;

public class SpringBeanResolver implements ApplicationContextAware, BeanResolver {
	private ApplicationContext context;
	
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;
	}
	
	@Override
	public <T> T resolve(Class<T> clazz) {
		return context.getBean(clazz);
	}

}
