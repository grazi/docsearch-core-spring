package codes.probst.docsearch.method.facade;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import codes.probst.docsearch.ApplicationConfiguration;
import codes.probst.docsearch.TestEntityManagerConfiguration;
import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.method.model.MethodUsageModel;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
	ApplicationConfiguration.class,
	TestEntityManagerConfiguration.class
})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class MethodFacadeTest {

	@Autowired
	private MethodFacade facade;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Before
	@Transactional
	public void setup() {
		ProjectModel project1 = insertProject("junit-group", "junit");
		ProjectModel project2 = insertProject("junit-group", "junit2");
		ProjectModel project3 = insertProject("junit-group2", "junit");
		
		VersionModel version1 = insertVersion("1.0.0", project1);
		VersionModel version2 = insertVersion("1.1.0", project1);
		VersionModel version3 = insertVersion("1.0.1", project1);
		VersionModel version4 = insertVersion("1.0", project2);
		VersionModel version5 = insertVersion("1.1", project3);
		
		PackageModel package1 = importPackage("com.example", version1);
		PackageModel package2 = importPackage("com.example", version2);
		package2.setDocumentation("Lorem ipsum. Dolor est.");
		persist(package2);
		
		PackageModel package3 = importPackage("com.example", version3);
		PackageModel package4 = importPackage("com.example.impl", version3);
		PackageModel package5 = importPackage("com.junit", version4);
		PackageModel package6 = importPackage("com.junit", version5);
		
		ClassModel clazz1 = importClass("Example", package1);
		clazz1.setDocumentation("Lorem ipsum. Dolor est.");
		clazz1.setSource("public class Example {\n}".getBytes(Charset.forName("utf-8")));
		persist(clazz1);
		importClass("Example", package2);
		importClass("Example", package3);
		importClass("ExampleImpl", package4);
		importClass("Test", package5);
		importClass("Test", package6);
		
		MethodModel method1 = importMethod("test", "test()", clazz1, clazz1);
		
		ClassUsageModel parameter1 = new ClassUsageModel();
		parameter1.setDocumentation("Lorem ipsum");
		parameter1.setClazz(clazz1);
		persist(parameter1);
		method1.setParameters(Arrays.asList(parameter1));
		persist(method1);
		method1.getReturnType().setDocumentation("Returns an Example");
		persist(method1.getReturnType());
		
		MethodUsageModel usage1 = new MethodUsageModel();
		usage1.setCallingClass(clazz1);
		usage1.setMethod(method1);
		usage1.setSnipped("\tpublic void test(){\n\\ttexample.test();");;
		usage1.setLineNumber(5);
		persist(usage1);
		
		
		importMethod("execute", "execute()", clazz1, clazz1);
	}
	
	@Test
	@Transactional
	public void testGetbyClassId() {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("id", SortDirection.ASCENDING) });
		Collection<MethodData> methods = facade.getByClassId(0l, pageable);
		Assert.assertNotNull(methods);
		Assert.assertEquals(2, methods.size());
		Iterator<MethodData> it = methods.iterator();
		
		{
			MethodData method = it.next();
			Assert.assertEquals("test", method.getName());
			Assert.assertEquals("Example test()", method.getSignature());
		}
		
		{
			MethodData method = it.next();
			Assert.assertEquals("execute", method.getName());
			Assert.assertEquals("Example execute()", method.getSignature());
		}
	}
	
	@Test
	@Transactional
	public void testGetByKey() {
		{
			Optional<MethodData> optional = facade.getByKey(0l);
			Assert.assertTrue(optional.isPresent());
			MethodData method = optional.get();
			Assert.assertEquals("test", method.getName());
			Assert.assertEquals("Example test()", method.getSignature());
		}

		{
			Optional<MethodData> optional = facade.getByKey(0l, MethodPopulationOption.OPTION_TAGS);
			Assert.assertTrue(optional.isPresent());
			MethodData method = optional.get();
			Assert.assertEquals("test", method.getName());
			Assert.assertEquals("Example test()", method.getSignature());
			Collection<String> parameterTypeTags = method.getParameterTypeTags();
			Assert.assertNotNull(parameterTypeTags);
			Assert.assertEquals(1, parameterTypeTags.size());
			Iterator<String> it = parameterTypeTags.iterator();
			Assert.assertEquals("Lorem ipsum", it.next());
			Assert.assertEquals("Returns an Example", method.getReturnTypeTag());
		}
		{
			Optional<MethodData> optional = facade.getByKey(0l, MethodPopulationOption.OPTION_REFERENCES);
			Assert.assertTrue(optional.isPresent());
			MethodData method = optional.get();
			Assert.assertEquals("test", method.getName());
			Assert.assertEquals("Example test()", method.getSignature());
			Collection<MethodReferenceData> references = method.getReferences();
			Assert.assertNotNull(references);
			Assert.assertEquals(1, references.size());
			MethodReferenceData reference = references.iterator().next();
			Assert.assertEquals("\tpublic void test(){\n\\ttexample.test();", reference.getSnipped());
			ClassData clazz = reference.getClazz();
			Assert.assertNotNull(clazz);
			Assert.assertEquals("Example", clazz.getName());
			Assert.assertEquals("/projects/junit/0/1.0.0/0/com.example/0/Example/0", clazz.getUrl());
		}
	}
	
	@Test
	@Transactional
	public void testGetAll() {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("id", SortDirection.ASCENDING) });
		Collection<MethodData> methods = facade.getAll(pageable);
		Assert.assertNotNull(methods);
		Iterator<MethodData> it = methods.iterator();
		
		{
			MethodData method = it.next();
			Assert.assertEquals("test", method.getName());
			Assert.assertEquals("Example test()", method.getSignature());
		}
		
		{
			MethodData method = it.next();
			Assert.assertEquals("execute", method.getName());
			Assert.assertEquals("Example execute()", method.getSignature());
		}
	}
	

	private MethodModel importMethod(String name, String signature, ClassModel returnType, ClassModel clazz) {
		ClassUsageModel returnTypeUsage = new ClassUsageModel();
		returnTypeUsage.setArray(false);
		returnTypeUsage.setClazz(returnType);
		persist(returnTypeUsage);
		
		MethodModel method = new MethodModel();
		method.setName(name);
		method.setSignature(signature);
		method.setReturnType(returnTypeUsage);
		method.setClazz(clazz);
		persist(method);
		return method;
	}

	private ClassModel importClass(String name, PackageModel packagez) {
		ClassModel clazz = new ClassModel();
		clazz.setName(name);
		clazz.setPackageModel(packagez);
		persist(clazz);
		return clazz;
	}
	
	private PackageModel importPackage(String name, VersionModel version) {
		PackageModel packagez = new PackageModel();
		packagez.setName(name);
		packagez.setVersion(version);
		persist(packagez);
		return packagez;
	}
	
	private VersionModel insertVersion(String code, ProjectModel project) {
		VersionModel version = new VersionModel();
		version.setCode(code);
		version.setProject(project);
		persist(version);
		return version;
	}
	
	private ProjectModel insertProject(String groupId, String artifactId) {
		ProjectModel project = new ProjectModel();
		project.setGroupId(groupId);
		project.setArtifactId(artifactId);
		persist(project);
		return project;
	}
	
	private void persist(Object object) {
		entityManager.persist(object);
	}
}
