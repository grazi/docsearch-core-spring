package codes.probst.docsearch.analyzer.projectinformation;

import java.io.IOException;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import codes.probst.docsearch.ApplicationConfiguration;
import codes.probst.docsearch.TestEntityManagerConfiguration;
import codes.probst.framework.file.service.FileService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
	ApplicationConfiguration.class,
	TestEntityManagerConfiguration.class
})
public class ProjectInformationAnalyzerTest {

	@Autowired
	private ProjectInformationAnalyzer analyzer;
	
	@Autowired
	private FileService fileService;
	
	@Test
	public void testMavenProject() throws IOException {
		Optional<ProjectInformation> optional = analyzer.analyze(fileService.getBytesFromInputStream(getClass().getResourceAsStream("/example1-0.0.1-SNAPSHOT.jar")));
		
		Assert.assertTrue(optional.isPresent());
		ProjectInformation info = optional.get();
		Assert.assertEquals("codes.probst", info.getGroupId());
		Assert.assertEquals("example1", info.getArtifactId());
		Assert.assertEquals("0.0.1-SNAPSHOT", info.getVersion());
		Assert.assertEquals(ProjectType.MAVEN, info.getType());
	}
	
	@Test
	public void testJavaProject() throws IOException {
		Optional<ProjectInformation> optional = analyzer.analyze(fileService.getBytesFromInputStream(getClass().getResourceAsStream("/java.zip")));
		
		Assert.assertTrue(optional.isPresent());
		ProjectInformation info = optional.get();
		Assert.assertEquals("java", info.getGroupId());
		Assert.assertEquals("jdk", info.getArtifactId());
		Assert.assertEquals("1.8", info.getVersion());
		Assert.assertEquals(ProjectType.JAVA, info.getType());
	}
}
