package codes.probst.docsearch.queue.service;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import codes.probst.docsearch.ApplicationConfiguration;
import codes.probst.docsearch.TestEntityManagerConfiguration;
import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.queue.dao.QueueEntryDao;
import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.docsearch.queue.model.QueueEntryState;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
	ApplicationConfiguration.class,
	TestEntityManagerConfiguration.class
})
public class QueueEntryServiceTest {
	@Autowired
	private QueueEntryService queueEntryService;
	
	@Autowired
	private QueueEntryDao queueEntryDao;
	
	@Test
	@Transactional
	public void testGetByDependency(){
		Optional<QueueEntryModel> optional = queueEntryService.getByDependency("junit-group", "junit", "1.0");
		
		Assert.assertNotNull(optional);
		Assert.assertFalse(optional.isPresent());
		insertTestRow("junit-group", "junit", "1.0", ProjectType.MAVEN);
		insertTestRow("junit-group", "junit", "2.0", ProjectType.MAVEN);
		insertTestRow("junit-group", "junit2", "1.0", ProjectType.MAVEN);
		insertTestRow("junit-group", "junit2", "2.0", ProjectType.MAVEN);
		insertTestRow("junit-grou2p", "junit", "1.0", ProjectType.MAVEN);
		
		optional = queueEntryService.getByDependency("junit-group", "junit", "1.0");
		
		Assert.assertNotNull(optional);
		Assert.assertTrue(optional.isPresent());
		
		QueueEntryModel model = optional.get();
		Assert.assertEquals("junit-group", model.getGroupId());
		Assert.assertEquals("junit", model.getArtifactId());
		Assert.assertEquals("1.0", model.getVersion());
		Assert.assertEquals(ProjectType.MAVEN, model.getType());
		Assert.assertArrayEquals(new byte[0], model.getCompiledData());
		Assert.assertArrayEquals(new byte[0], model.getSourceData());
		Assert.assertEquals(QueueEntryState.NEW, model.getState());
	}
	
	private void insertTestRow(String groupId, String artifactId, String version, ProjectType type) {
		QueueEntryModel value = new QueueEntryModel();
		
		value.setGroupId(groupId);
		value.setArtifactId(artifactId);
		value.setVersion(version);
		value.setCompiledData(new byte[0]);
		value.setSourceData(new byte[0]);
		value.setType(type);
		value.setState(QueueEntryState.NEW);
		
		queueEntryDao.insert(value);
	}
}
