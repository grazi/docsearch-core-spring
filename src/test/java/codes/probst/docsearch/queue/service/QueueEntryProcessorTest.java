package codes.probst.docsearch.queue.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import codes.probst.docsearch.ApplicationConfiguration;
import codes.probst.docsearch.TestEntityManagerConfiguration;
import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.clazz.dao.ClassDao;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.docsearch.method.dao.MethodDao;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.packages.dao.PackageDao;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.dao.ProjectDao;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.docsearch.queue.model.QueueEntryState;
import codes.probst.docsearch.version.dao.VersionDao;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.file.service.FileService;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
	ApplicationConfiguration.class,
	TestEntityManagerConfiguration.class
})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class QueueEntryProcessorTest {
	@Autowired
	private QueueEntryProcessor processor;
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private ClassDao classDao;
	
	@Autowired
	private MethodDao methodDao;
	
	@Autowired
	private PackageDao packageDao;
	
	@Autowired
	private VersionDao versionDao;
	
	@Autowired
	private ProjectDao projectDao;
	
	@Test
	@Transactional
	public void testJavaLite() throws IOException {
		QueueEntryModel queueEntry = new QueueEntryModel();
		queueEntry.setArtifactId("jdk");
		queueEntry.setGroupId("java");
		queueEntry.setVersion("1.8");
		queueEntry.setCompiledData(fileService.getBytesFromInputStream(getClass().getResourceAsStream("/java-lite.jar")));
		queueEntry.setSourceData(new byte[0]);
		queueEntry.setType(ProjectType.JAVA);
		
		processor.process(queueEntry);
		
		Assert.assertEquals(QueueEntryState.SUCCESSFULLY, queueEntry.getState());
		
		{
			Optional<ClassModel> optionalClass = classDao.findByNameAndPackageInVersions("Object", "java.lang", Arrays.asList(0l));
			
			Assert.assertTrue(optionalClass.isPresent());
			
			ClassModel clazz = optionalClass.get();
			Assert.assertEquals("Object", clazz.getName());
			
			Optional<PackageModel> optionalPackage = packageDao.findByClassId(clazz.getId());
			Assert.assertTrue(optionalPackage.isPresent());
			PackageModel packagez = optionalPackage.get();
			Assert.assertNotNull(packagez);
			Assert.assertEquals("java.lang", packagez.getName());
			
			Optional<VersionModel> optionalVersion = versionDao.findByClassId(clazz.getId());
			Assert.assertTrue(optionalVersion.isPresent());
			VersionModel version = optionalVersion.get();
			Assert.assertNotNull(version);
			Assert.assertEquals("1.8", version.getCode());
			
			Optional<ProjectModel> optionalProject = projectDao.findByClassId(clazz.getId());
			Assert.assertTrue(optionalProject.isPresent());
			ProjectModel project = optionalProject.get();
			Assert.assertNotNull(project);
			Assert.assertEquals("java", project.getGroupId());
			Assert.assertEquals("jdk", project.getArtifactId());
			
			Pageable pageable = new Pageable();
			pageable.setSort(new Sort[] { new Sort("signature", SortDirection.ASCENDING) });
			Collection<MethodModel> methods = methodDao.findByClassId(clazz.getId(), pageable);
			Assert.assertEquals(12, methods.size());
			Assert.assertNotNull(methods);
			
			Iterator<MethodModel> it = methods.iterator();
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("clone", method.getName());
				Assert.assertEquals("clone()", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("Object", method.getReturnType().getClazz().getName());
				Assert.assertEquals(0, method.getParameters().size());
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("equals", method.getName());
				Assert.assertEquals("equals(java.lang.Object)", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("boolean", method.getReturnType().getClazz().getName());
				Assert.assertEquals(1, method.getParameters().size());
				
				Iterator<ClassUsageModel> cit = method.getParameters().iterator();
				
				{
					ClassModel parameterClazz = cit.next().getClazz();
					Assert.assertEquals("Object", parameterClazz.getName());
				}
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("finalize", method.getName());
				Assert.assertEquals("finalize()", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("void", method.getReturnType().getClazz().getName());
				Assert.assertEquals(0, method.getParameters().size());
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("getClass", method.getName());
				Assert.assertEquals("getClass()", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("Class", method.getReturnType().getClazz().getName());
				Assert.assertEquals(0, method.getParameters().size());
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("hashCode", method.getName());
				Assert.assertEquals("hashCode()", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("int", method.getReturnType().getClazz().getName());
				Assert.assertEquals(0, method.getParameters().size());
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("notify", method.getName());
				Assert.assertEquals("notify()", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("void", method.getReturnType().getClazz().getName());
				Assert.assertEquals(0, method.getParameters().size());
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("notifyAll", method.getName());
				Assert.assertEquals("notifyAll()", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("void", method.getReturnType().getClazz().getName());
				Assert.assertEquals(0, method.getParameters().size());
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("registerNatives", method.getName());
				Assert.assertEquals("registerNatives()", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("void", method.getReturnType().getClazz().getName());
				Assert.assertEquals(0, method.getParameters().size());
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("toString", method.getName());
				Assert.assertEquals("toString()", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("String", method.getReturnType().getClazz().getName());
				Assert.assertEquals(0, method.getParameters().size());
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("wait", method.getName());
				Assert.assertEquals("wait()", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("void", method.getReturnType().getClazz().getName());
				Assert.assertEquals(0, method.getParameters().size());
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("wait", method.getName());
				Assert.assertEquals("wait(long)", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("void", method.getReturnType().getClazz().getName());
				Assert.assertEquals(1, method.getParameters().size());

				Iterator<ClassUsageModel> cit = method.getParameters().iterator();
				
				{
					ClassModel parameterClazz = cit.next().getClazz();
					Assert.assertEquals("long", parameterClazz.getName());
				}
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("wait", method.getName());
				Assert.assertEquals("wait(long, int)", method.getSignature());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("void", method.getReturnType().getClazz().getName());
				Assert.assertEquals(2, method.getParameters().size());

				Iterator<ClassUsageModel> cit = method.getParameters().iterator();
				
				{
					ClassModel parameterClazz = cit.next().getClazz();
					Assert.assertEquals("long", parameterClazz.getName());
				}
				
				{
					ClassModel parameterClazz = cit.next().getClazz();
					Assert.assertEquals("int", parameterClazz.getName());
				}
			}
			
		}
	}
	
	
	@Test
	@Transactional
	public void testMavenExample() throws IOException {
		QueueEntryModel queueEntry = new QueueEntryModel();
		queueEntry.setArtifactId("jdk");
		queueEntry.setGroupId("java");
		queueEntry.setVersion("1.8");
		queueEntry.setCompiledData(fileService.getBytesFromInputStream(getClass().getResourceAsStream("/java-lite.jar")));
		queueEntry.setSourceData(new byte[0]);
		queueEntry.setType(ProjectType.JAVA);
		
		processor.process(queueEntry);
		Assert.assertEquals(QueueEntryState.SUCCESSFULLY, queueEntry.getState());
		
		queueEntry = new QueueEntryModel();
		queueEntry.setArtifactId("example1");
		queueEntry.setGroupId("codes.probst");
		queueEntry.setVersion("0.0.1-SNAPSHOT");
		queueEntry.setCompiledData(fileService.getBytesFromInputStream(getClass().getResourceAsStream("/example1-0.0.1-SNAPSHOT.jar")));
		queueEntry.setSourceData(fileService.getBytesFromInputStream(getClass().getResourceAsStream("/example1-0.0.1-SNAPSHOT-sources.jar")));
		queueEntry.setType(ProjectType.MAVEN);

		processor.process(queueEntry);
		Assert.assertEquals(QueueEntryState.SUCCESSFULLY, queueEntry.getState());
		
		{
			Optional<ClassModel> optionalClass = classDao.findByNameAndPackageInVersions("Example", "codes.probst.example1", Arrays.asList(1l));
			
			Assert.assertTrue(optionalClass.isPresent());
			
			ClassModel clazz = optionalClass.get();
			Assert.assertEquals("Example", clazz.getName());
			Assert.assertEquals("Class Doc", clazz.getDocumentation());
			
			Optional<PackageModel> optionalPackage = packageDao.findByClassId(clazz.getId());
			Assert.assertTrue(optionalPackage.isPresent());
			PackageModel packagez = optionalPackage.get();
			Assert.assertNotNull(packagez);
			Assert.assertEquals("codes.probst.example1", packagez.getName());
			
			Optional<VersionModel> optionalVersion = versionDao.findByClassId(clazz.getId());
			Assert.assertTrue(optionalVersion.isPresent());
			VersionModel version = optionalVersion.get();
			Assert.assertNotNull(version);
			Assert.assertEquals("0.0.1-SNAPSHOT", version.getCode());
			
			Optional<ProjectModel> optionalProject = projectDao.findByClassId(clazz.getId());
			Assert.assertTrue(optionalProject.isPresent());
			ProjectModel project = optionalProject.get();
			Assert.assertNotNull(project);
			Assert.assertEquals("codes.probst", project.getGroupId());
			Assert.assertEquals("example1", project.getArtifactId());
			
			Pageable pageable = new Pageable();
			pageable.setSort(new Sort[] { new Sort("signature", SortDirection.ASCENDING) });
			Collection<MethodModel> methods = methodDao.findByClassId(clazz.getId(), pageable);
			Assert.assertEquals(2, methods.size());
			Assert.assertNotNull(methods);
			
			Iterator<MethodModel> it = methods.iterator();
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("main", method.getName());
				Assert.assertEquals("main(codes.probst.example1.Test[])", method.getSignature());
				Assert.assertEquals("Method doc", method.getDocumentation());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("void", method.getReturnType().getClazz().getName());
				Assert.assertEquals(1, method.getParameters().size());

				Iterator<ClassUsageModel> cit = method.getParameters().iterator();
				
				{
					ClassUsageModel parameterUsage = cit.next();
					ClassModel parameterClazz = parameterUsage.getClazz();
					Assert.assertEquals("Test", parameterClazz.getName());
					Assert.assertEquals("args", parameterUsage.getDocumentation());
				}
			}
			
			{
				MethodModel method = it.next();
				Assert.assertEquals("test", method.getName());
				Assert.assertEquals("test(codes.probst.example1.Test)", method.getSignature());
				Assert.assertEquals("Test doc", method.getDocumentation());
				Assert.assertNotNull(method.getReturnType());
				Assert.assertNotNull(method.getReturnType().getClazz());
				Assert.assertEquals("Test", method.getReturnType().getClazz().getName());
				Assert.assertEquals("The test", method.getReturnType().getDocumentation());
				Assert.assertEquals(1, method.getParameters().size());

				Iterator<ClassUsageModel> cit = method.getParameters().iterator();
				
				{
					ClassUsageModel parameterUsage = cit.next();
					ClassModel parameterClazz = parameterUsage.getClazz();
					Assert.assertEquals("Test", parameterClazz.getName());
					Assert.assertEquals("test A test", parameterUsage.getDocumentation());
				}
			}
			
			
		}
	}
	
}
